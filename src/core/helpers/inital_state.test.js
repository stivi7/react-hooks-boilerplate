import { getInitialState } from './initial_state';

test('Test initial state', () => {
  expect(getInitialState()).toStrictEqual({
    isFetching: false,
    isCreating: false,
    isUpdating: false,
    isDeleting: false,
    error: undefined,
    _data: {},
  });
});
