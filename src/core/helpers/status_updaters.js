/* eslint-disable no-param-reassign */
import { action } from 'easy-peasy';
import types from './action_types';

/**
 * Status Updaters
 * @param {object} params
 * @param {string} params.type FETCH, UPDATE, CREATE, DELETE
 * @param {function} params.setData Callback which provide _data as argument, returns a new data
 * @param {object or string}
 */

export const start = action((state, params) => {
  if (!params || !params.type) {
    throw new Error('Missing arguments');
  }

  const { type, setData } = params;

  switch (type) {
    case types.FETCH:
      state.isFetching = true;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.CREATE:
      state.isCreating = true;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.UPDATE:
      state.isUpdating = true;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.DELETE:
      state.isDeleting = true;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    default:
      break;
  }
});

export const success = action((state, params) => {
  if (!params || !params.type) {
    throw new Error('Missing arguments');
  }

  const { type, setData } = params;

  switch (type) {
    case types.FETCH:
      state.isFetching = false;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.CREATE:
      state.isCreating = false;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.UPDATE:
      state.isUpdating = false;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.DELETE:
      state.isDeleting = false;
      state.error = undefined;
      state._data = setData ? setData(state._data) : state._data;
      break;
    default:
      break;
  }
});

export const fail = action((state, params) => {
  if (!params || !params.type) {
    throw new Error('Missing arguments');
  }

  const { type, setData, error } = params;

  switch (type) {
    case types.FETCH:
      state.isFetching = false;
      state.error = error;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.CREATE:
      state.isCreating = false;
      state.error = error;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.UPDATE:
      state.isUpdating = false;
      state.error = error;
      state._data = setData ? setData(state._data) : state._data;
      break;
    case types.DELETE:
      state.isDeleting = false;
      state.error = error;
      state._data = setData ? setData(state._data) : state._data;
      break;
    default:
      break;
  }
});
