export const getInitialState = () => ({
  isFetching: false,
  isCreating: false,
  isUpdating: false,
  isDeleting: false,
  error: undefined,
  _data: {},
});
