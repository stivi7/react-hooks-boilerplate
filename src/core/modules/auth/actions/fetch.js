/* eslint-disable no-param-reassign */
import { thunk, action } from 'easy-peasy';
import { saveItem } from '../../../../utils/storage';
import storageKeys from '../../../../constants/storage';

export const login = thunk(async (actions, params) => {
  try {
    console.log('STARTED');
    actions.start({ type: 'FETCH' });

    setTimeout(() => {
      console.log('SUCCESS');
      saveItem(storageKeys.USER, params);
      actions.success({
        type: 'FETCH',
        setData: data => ({
          ...data,
          user: params,
        }),
      });
    }, 1000);
    return params;
  } catch (error) {
    throw new Error(error);
  }
});

export const setUser = action((state, payload) => {
  const data = {
    ...state._data,
    user: payload,
  };

  state._data = data;
});
