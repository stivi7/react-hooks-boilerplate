import { getInitialState } from '../../helpers/initial_state';
import { login, setUser } from './actions/fetch';
import * as statusUpdaters from '../../helpers/status_updaters';

export default {
  auth: {
    ...getInitialState(),

    // start, success, fail
    ...statusUpdaters,
    login,
    setUser,
  },
};
