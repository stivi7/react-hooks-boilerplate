import authState from './modules/auth/state';

const state = {
  // modules state
  ...authState,
};

export default state;
