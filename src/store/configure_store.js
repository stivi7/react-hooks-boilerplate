import { createStore } from 'easy-peasy';
import storeModel from '../core'

const store = createStore(storeModel);

export default store;