export const saveItem = (key, value) => localStorage.setItem(key, JSON.stringify(value));

export const deleteItem = key => localStorage.removeItem(key);

export const getItem = key => JSON.parse(localStorage.getItem(key));
