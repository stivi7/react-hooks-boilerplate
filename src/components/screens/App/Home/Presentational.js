import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

const HomeScreen = () => (
    <Fragment>
      <h1>Home</h1>
      <Switch>
        <Route exact path="/" render={() => <h1>Content</h1>} />
        <Route exact path="/home1" render={() => <h1>test</h1>} />
        <Route render={() => <h1>Not Found</h1>} />
      </Switch>
    </Fragment>
);

export default HomeScreen;
