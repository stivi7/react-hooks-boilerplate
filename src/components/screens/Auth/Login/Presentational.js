import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useActions } from 'easy-peasy';

const LoginScreen = (props) => {
  const login = useActions(actions => actions.auth.login);
  const { history } = props;

  return (
    <Fragment>
      <h1>Login Page</h1>
      <button
        onClick={async () => {
          // Simulate success login and navigation
          login({ name: 'Stivi', email: 'stivi.ndoni@softup.co' });
          setTimeout(() => history.replace('/'), 1100);
        }}
      >
        Login
      </button>
    </Fragment>
  );
};

LoginScreen.propTypes = {
  history: PropTypes.object,
};

export default LoginScreen;
