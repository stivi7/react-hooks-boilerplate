import React, { useEffect, useState } from 'react';
import { useActions } from 'easy-peasy';
import { Switch, Route, withRouter } from 'react-router-dom';

import { AppContainer } from './App.styled';
import LoginScreen from './screens/Auth/Login/Presentational';
import HomeScreen from './screens/App/Home/Presentational';
import { getItem } from '../utils/storage';
import storageKeys from '../constants/storage';


const App = ({ history, location }) => {
  const [authenticating, setAuthenticating] = useState(true);
  const setUser = useActions(actions => actions.auth.setUser);

  useEffect(() => {
    const user = getItem(storageKeys.USER);
    if (user) {
      setAuthenticating(false);
      if (!authenticating) {
        const path = location.pathname === '/login' ? '/' : location.pathname;
        setUser(user);
        history.replace(path);
      }
    } else {
      setAuthenticating(false);
      if (!authenticating) {
        history.replace('/login');
      }
    }
  }, [authenticating, history, location.pathname, setUser]);

  return (
    <AppContainer>
      {authenticating ? 'AUTHENTICATING...' : (
        <Switch>
          <Route exact path="/login" component={LoginScreen} />
          <Route path="/" component={HomeScreen} />
          <Route render={() => <h1>Not Found</h1>} />
        </Switch>
      )}
    </AppContainer>
  );
};

export default withRouter(App);
