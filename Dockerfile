FROM node
RUN mkdir /app
VOLUME [ "/app" ]
WORKDIR /app
EXPOSE 3000
RUN rm -rf node_modules
RUN npm install
CMD ["npm", "run", "run:prod" ]
